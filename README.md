# Support de phare pour TD

Modèle OpenSCAD d'un support de phare avant pour vélo couché à traction directe.

## Usage
A imprimer (en 3D), résistance correcte avec un remplissage à 100%

## License
CC0
