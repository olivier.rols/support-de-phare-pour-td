/* distance boitier entretoise */
LA = 21.4;

/* boitier pédalier et tube de selle */
DA2 = 40.4;
module boitierPedalier(){
translate([LA/2+DA2/2,0,0]){
    rotate([90,0,0]) {
        cylinder(h=70,r=DA2/2, center=true);
    }}
translate([LA/2+DA2/2,0,0]){
    rotate([0,-30,0]) {
        cylinder(h=40,r=32/2, center=false);
    }}
}


/* bases */
DB = 26;
LC1 = 13.6;
LC2 = 21.6;
LC3 = LA;
/* pontet bases */
DA1g = 12;
DA1p = 7.5;
HA1 = 22.1;
alpha = atan2(-(LC2-LC1)/2,LC3);
    
module moitie(){
    /* bases */
    translate([-LC3+DA1g/2,0,0]){
        translate([0,-LC2/2-DB/2+DA1g/2*tan(alpha),0]){
            rotate([0,90,0]){
                rotate([alpha,0,0]){
                    cylinder(h=LC3+DA1g,r=DB/2);}}}}
    /* pontet bases */
    translate([-LA/2+-DA1p/2,0,0]){
        rotate([90,0,0]){
            union(){
                translate([0,0,HA1/2]){
                       cylinder(h=DB/2,r=DA1g/2,center=false);}
                cylinder(h=HA1/2,r1=DA1p/2,r2=DA1g/2,center=false);
            }}}
}        
        

/* support fixation */
grandL = LC3+DA2/2+DA1g/2;
petitL = 21;
haut = 20;
module demisupport(){
    rotate([0,0,-alpha]){
        translate([LC3/2,-petitL/8,0]){
            cube(size=[grandL,petitL/2,haut],center=true);
        }
    }
}

hauteurplaque=10;
module plaquereparation(){
    translate([0,0,haut/2]){
        cube(size=[20,LC2+DB,hauteurplaque],center=true);}
}

module vis1(){
    cylinder(h=2*haut+hauteurplaque,r=4.2/2,center=true);
    translate([0,0,hauteurplaque]){
        //cylinder(h=5.1,r=10/2,center=false);
        // écrou de 8
        poly_n=6;
        rotate([0,0,90]){
        cylinder(h=5.1+hauteurplaque,r=8/2/cos(180/poly_n),center=false,$fn=poly_n);}
    }
    translate([0,0,-22.1]){
        cylinder(h=17.1,r=12/2,center=false);
    }
}

/* support phare*/
module vis2(){
    union(){
        cylinder(h=3*hauteur,r=5.2/2,center=true);
        // écrou de 8
        poly_n=6;
        rotate([0,0,30]){
        cylinder(h=5,r=8/2/cos(180/poly_n),center=false,$fn=poly_n);}
    }
}

module phare(){
    // évidement pour montage avec liberté de rotation
    // à faire
}

beta=atan2((DA2-DA1g)/2,LA+(DA2+DA1g)/2);
largeur = 15;
longueur = LA+(DA2+DA1g)/2;
hauteur = 15;
hauteur2 = 20;
module supportPhare(){
    rotate([0,0*2*beta+45,0]){
        translate([0,-largeur/2,-hauteur2/2-1]){
            cube(size=[longueur-largeur-2,largeur,hauteur+5],center=false);
            translate([longueur-largeur/2,largeur/2,0]){
                difference() {
                    cylinder(h=hauteur+5,r=largeur,center=false);
                    translate([0,0,hauteur+1]){
                    vis2();}
                }
            }
        }
    }
}

module plaque(){
    mirror([0,1,0]){ demisupport();}
    demisupport();
    plaquereparation();
    supportPhare();
    }
module deuxplaques(){
    difference (){
        plaque();
        cube(size=[2*grandL,2*petitL,2],center=true);
    }
}
module supportcomplet() {
    difference(){
        deuxplaques();
        vis1();
    }
}

module cadre () {
    mirror([0,1,0]){ moitie();}
    moitie();
    boitierPedalier();
}

difference (){
    supportcomplet();
    cadre();
}
//cube(size=[2*grandL,2*petitL,2],center=true);

//boitierPedalier();
//moitie();



